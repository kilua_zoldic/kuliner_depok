<?php

/**
 * Created by PhpStorm.
 * User: rusdi
 * Date: 10/18/15
 * Time: 9:23 PM
 */
Class KDModels extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
//        $this->load->library('ci_redis');

    }

    public function updateTblAjax($table, $data = array(), $codition = array())
    {
        $update = $this->db->update($table, $data, $codition);

        if ($update) {
            $data['response'] = true;
            $data['msg'] = 'Update data ' . $table . ' success';
        } else {
            $data['response'] = false;
            $data['msg'] = 'Update Data ' . $table . ' failed';
            print_r($this->db);
            exit;
        }
        return $data;
    }


    public function updateTbl($table, $data = array(), $codition = array())
    {
        $this->db->update($table, $data, $codition);

        if ($this->db->affected_rows() > 0) {
            $data['response'] = true;
            $data['msg'] = 'Update data ' . $table . ' success';
        } else {
            $data['response'] = false;
            $data['msg'] = 'Update Data ' . $table . ' failed';
            print_r($this->db);
            exit;
        }
        return $data;
    }

    public function selectTbl($table, $condition = array(), $limit = null, $offset = null)
    {

        $query = $this->db->get_where($table, $condition, $limit, $offset)->result_array();
        return $query;

    }

    public function selectTblQuery($table, $condition = "", $limit = null, $offset = null, $oderby = "")
    {
        $query = $this->db->from($table)
            ->where($condition)
            ->order_by($oderby)
            ->limit($limit, $offset)
            ->get()
            ->result_array();

        return $query;

    }

    public function selectTblJoin($table, $join = array(), $condition = array(), $limit = null, $offset = null )
    {
        $this->db->from($table);
        foreach($join as $key){
            $key;
        }
        foreach($condition as $key){
            $key;
        }
        $query = $this->db->limit($limit, $offset)->get();
        return $query->result_array();
    }


    public function insertTbl($table, $data)
    {
        $this->db->insert($table, $data);

        if ($this->db->affected_rows() > 0) {
            $data['response'] = true;
            $data['insert_id'] = $this->db->insert_id();
            $data['msg'] = 'Insert data ' . $table . ' success';
        } else {
            $data['response'] = false;
            $data['msg'] = 'Insert data ' . $table . ' failed';
        }
        return $data;
    }


    public function deleteTbl($table, $codition = array())
    {
        $this->db->delete($table,$codition);

        if ($this->db->affected_rows() > 0) {
            $data['response'] = true;
            $data['msg'] = 'Delete Data ' . $table . ' success';
        } else {
            $data['response'] = false;
            $data['msg'] = 'Delete Data ' . $table . ' failed';
            print_r($this->db);
            exit;
        }
        return $data;
    }


    public function sumTbl($field, $table){
        $this->db->select_sum($field);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            $response['data'] = $query->result_array();
            $response['status'] = true;
            return $response;
        } else {
            $response['data'] = 'NULL';
            $response['status'] = false;
            return $response;
        }
    }

}