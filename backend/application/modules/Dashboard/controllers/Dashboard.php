<?php
/**
 * Created by PhpStorm.
 * User: agengmaulana
 * Date: 1/23/17
 * Time: 12:16 AM
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends KDControllers {

    public function __construct() {
        parent::__construct();

    }

    public function index(){
        $this->load->view('Dashboard');
    }

}